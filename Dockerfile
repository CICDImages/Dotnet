FROM debian:stretch-slim

# Install Mono

RUN apt-get update && apt-get install -y gnupg2

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

RUN echo "deb http://download.mono-project.com/repo/debian stretch/snapshots/5.10.0.140 main" > /etc/apt/sources.list.d/mono-official-stable.list \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
        git \
        binutils \
        curl \
        libc6 \
        libcurl3 \
        libgcc1 \
        libgssapi-krb5-2 \
        libicu57 \
        liblttng-ust0 \
        libssl1.0.2 \
        libstdc++6 \
        libunwind8 \
        libuuid1 \
        zlib1g

RUN apt-get install -y \
        mono-runtime \
        mono-devel \
        ca-certificates-mono \
        nuget \
        referenceassemblies-pcl \
  && rm -rf /var/lib/apt/lists/*

# Install NUnit Console, with version 2.6 and 3.x runners.

RUN nuget install NUnit.Console

# Install SharpCover.

RUN git clone https://github.com/Didstopia/SharpCoverPlus.git \
    && cd SharpCoverPlus \
    && nuget restore \
    && msbuild /p:Configuration=Release \
    && mkdir -p /opt/SharpCoverPlus \
    && cp /SharpCoverPlus/SharpCoverPlus/bin/Release/* /opt \
    && rm -rf /SharpCoverPlus \
    && rm /opt/Mono.Cecil.Mdb.dll

# Install .NET CLI dependencies
ENV DOTNET_SDK_VERSION 2.1.300-preview1-008174

# Install .NET Core SDK

RUN curl -SL --output dotnet.tar.gz https://dotnetcli.blob.core.windows.net/dotnet/Sdk/$DOTNET_SDK_VERSION/dotnet-sdk-$DOTNET_SDK_VERSION-linux-x64.tar.gz \
    && dotnet_sha512='3030d6876eef6770c54e6d1e23f1be544b72dfe171915d2e55e00e40faacec0035fe4f9d72a6dc5fc5fb29b768ff64c57dcce0b9fddd8f1b7f7ce8389f535da9' \
    && echo "$dotnet_sha512 dotnet.tar.gz" | sha512sum -c - \
    && mkdir -p /usr/share/dotnet \
    && tar -zxf dotnet.tar.gz -C /usr/share/dotnet \
    && rm dotnet.tar.gz \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet

ENV NUGET_XMLDOC_MODE skip

# Trigger first run experience by running arbitrary cmd to populate local package cache
RUN dotnet help

WORKDIR /usr/src
